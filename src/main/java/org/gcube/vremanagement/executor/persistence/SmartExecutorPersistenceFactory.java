/**
 * 
 */
package org.gcube.vremanagement.executor.persistence;

import java.util.HashMap;
import java.util.Map;

import org.gcube.vremanagement.executor.ContextUtility;
import org.gcube.vremanagement.executor.persistence.orientdb.OrientDBPersistenceConnector;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Luca Frosini (ISTI - CNR)
 *
 */
public abstract class SmartExecutorPersistenceFactory {

	private static final Logger logger = LoggerFactory.getLogger(SmartExecutorPersistenceFactory.class);

	private static Map<String, SmartExecutorPersistenceConnector> persistenceConnectors;

	static {
		persistenceConnectors = new HashMap<String, SmartExecutorPersistenceConnector>();
	}

	private static synchronized SmartExecutorPersistenceConnector getPersistenceConnector(String context) throws Exception {
		if (context == null) {
			String error = "No Context available.";
			logger.error(error);
			throw new RuntimeException(error);
		}

		logger.trace("Retrieving {} for context {}", SmartExecutorPersistenceConnector.class.getSimpleName(), context);

		SmartExecutorPersistenceConnector persistence = persistenceConnectors.get(context);

		if (persistence == null) {
			logger.trace("Retrieving {} for context {} not found on internal {}. Intializing it.",
					SmartExecutorPersistenceConnector.class.getSimpleName(), context, Map.class.getSimpleName());

			String className = OrientDBPersistenceConnector.class.getSimpleName();
			SmartExecutorPersistenceConfiguration configuration = new SmartExecutorPersistenceConfiguration(className);

			persistence = new OrientDBPersistenceConnector(configuration);
			persistenceConnectors.put(ContextUtility.getCurrentContext(), persistence);
		}

		return persistence;
	}

	/**
	 * @return the persistenceConnector
	 */
	public static SmartExecutorPersistenceConnector getPersistenceConnector() throws Exception {
		String context = ContextUtility.getCurrentContext();
		return getPersistenceConnector(context);
	}

	public static void closeCurrentPersistenceConnector() throws Exception {
		String context = ContextUtility.getCurrentContext();
		closePersistenceConnector(context);
	}

	private static synchronized void closePersistenceConnector(String context) throws Exception {
		SmartExecutorPersistenceConnector persistence = getPersistenceConnector(context);
		if (persistence != null) {
			persistence.close();
			persistenceConnectors.remove(context);
			if(persistenceConnectors.isEmpty()) {
				
			}
		}
	}

	public static void closeAll() {
		for (String context : persistenceConnectors.keySet()) {
			try {
				closePersistenceConnector(context);
			} catch (Exception e) {
				logger.error("Unable to close {} for context {}",
						SmartExecutorPersistenceConnector.class.getSimpleName(), context);
			}
		}
	}
	
	public static void shutdown() {
		OrientDBPersistenceConnector.shutdown();
	}

}
