package org.gcube.vremanagement.executor;

import org.gcube.common.authorization.utils.manager.SecretManagerProvider;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public class ContextUtility {
	
	public static String getCurrentContext() {
		return SecretManagerProvider.instance.get().getContext();
	}

}
