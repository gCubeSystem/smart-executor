package org.gcube.vremanagement.executor.ispublisher;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.gcube.smartgears.context.application.ApplicationContext;
import org.gcube.vremanagement.executor.plugin.Plugin;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public abstract class ISPublisher {
	
	protected static List<ISPublisher> isPublishers;

	public synchronized static List<ISPublisher> getISPublishers(ApplicationContext applicationContext){
		if(isPublishers==null) {
			isPublishers = new ArrayList<>();
			isPublishers.add(new GCoreISPublisher(applicationContext));
			// isPublishers.add(new RestISPublisher(applicationContext));
		}
		return isPublishers;
	}
	
	protected ApplicationContext applicationContext;
	
	public ISPublisher(ApplicationContext applicationContext) {
		this.applicationContext = applicationContext;
	}
	
	public abstract void publishPlugins(Map<String, Class<? extends Plugin>> availablePlugins) throws Exception;
	
	public abstract void unpublishPlugins(boolean force) throws Exception ;
	
}
