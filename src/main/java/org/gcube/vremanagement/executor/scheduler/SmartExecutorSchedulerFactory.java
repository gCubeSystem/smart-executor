package org.gcube.vremanagement.executor.scheduler;

import java.util.HashMap;
import java.util.Map;

import org.gcube.vremanagement.executor.ContextUtility;
import org.gcube.vremanagement.executor.persistence.SmartExecutorPersistenceConnector;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.SchedulerFactory;
import org.quartz.impl.StdSchedulerFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SmartExecutorSchedulerFactory {

	private static Logger logger = LoggerFactory.getLogger(SmartExecutorScheduler.class);
	
	private static Map<String, SmartExecutorScheduler> smartExecutorSchedulers = new HashMap<>();

	protected static SchedulerFactory schedulerFactory;
	
	static {
		schedulerFactory = new StdSchedulerFactory();
		smartExecutorSchedulers = new HashMap<>();
	}
	
	private static SmartExecutorScheduler getSmartExecutorScheduler(String context) throws SchedulerException {
		if(context==null){
			String error = "No context available.";
			logger.error(error);
			throw new RuntimeException(error); 
		}

		logger.trace("Retrieving {} for scope {}", 
				SmartExecutorPersistenceConnector.class.getSimpleName(), context);
		
		SmartExecutorScheduler smartExecutorScheduler = smartExecutorSchedulers.get(context);
		
		if(smartExecutorScheduler==null){
			logger.trace("Retrieving {} for scope {} not found on internal {}. Intializing it.", 
					SmartExecutorScheduler.class.getSimpleName(), 
					context, Map.class.getSimpleName());
			
			Scheduler scheduler = schedulerFactory.getScheduler();
			smartExecutorScheduler = new SmartExecutorScheduler(scheduler);
			
			smartExecutorSchedulers.put(ContextUtility.getCurrentContext(), 
					smartExecutorScheduler);
		}
		
		return smartExecutorScheduler;
	}
	
	/**
	 * @return the persistenceConnector
	 * @throws SchedulerException 
	 */
	public static synchronized SmartExecutorScheduler getSmartExecutorScheduler() throws SchedulerException {
		String context = ContextUtility.getCurrentContext();
		return getSmartExecutorScheduler(context);
	}
	
	public static void removeCurrentSmartExecutorScheduler(){
		String context = ContextUtility.getCurrentContext();
		smartExecutorSchedulers.remove(context);
	}
	
}
