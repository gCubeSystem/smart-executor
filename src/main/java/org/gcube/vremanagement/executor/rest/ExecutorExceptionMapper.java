package org.gcube.vremanagement.executor.rest;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import org.gcube.vremanagement.executor.exception.InputsNullException;
import org.gcube.vremanagement.executor.exception.InvalidInputsException;
import org.gcube.vremanagement.executor.exception.PluginInstanceNotFoundException;
import org.gcube.vremanagement.executor.exception.PluginNotFoundException;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
@Provider
public class ExecutorExceptionMapper implements ExceptionMapper<Exception> {
	
	@Override
	public Response toResponse(Exception exception) {
		
		Status status = Status.INTERNAL_SERVER_ERROR;
		String exceptionMessage = exception.getMessage();
		MediaType mediaType = MediaType.TEXT_PLAIN_TYPE;
		
		Class<? extends Exception> clz = exception.getClass();
		
		if(WebApplicationException.class.isAssignableFrom(clz)) {
			Response gotResponse = ((WebApplicationException) exception).getResponse();
			status = Status.fromStatusCode(gotResponse.getStatusInfo().getStatusCode());
		}
		
		if(PluginInstanceNotFoundException.class.isAssignableFrom(clz)|| PluginNotFoundException.class.isAssignableFrom(clz)) {
			status = Status.NOT_FOUND;
		}
		
		if(InputsNullException.class.isAssignableFrom(clz) || InvalidInputsException.class.isAssignableFrom(clz)) {
			status = Status.BAD_REQUEST;
		}
		
		return Response.status(status).entity(exceptionMessage).type(mediaType).build();
	}
	
}
