package org.gcube.vremanagement.executor;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.MediaType;

import org.gcube.smartgears.annotations.ManagedBy;
import org.gcube.vremanagement.executor.rest.RestSmartExecutor;
import org.glassfish.jersey.server.ResourceConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
@ApplicationPath("/")
@ManagedBy(SmartExecutorInitializator.class)
public class ResourceInitializer extends ResourceConfig {
	
	public static final String APPLICATION_JSON_CHARSET_UTF_8 = MediaType.APPLICATION_JSON + ";charset=UTF-8";
	
	private static Logger logger = LoggerFactory.getLogger(ResourceInitializer.class);
	
	public ResourceInitializer() {
		logger.info("Initializing smart-executor REST service");
		packages(RestSmartExecutor.class.getPackage().toString());
	}
	
}
