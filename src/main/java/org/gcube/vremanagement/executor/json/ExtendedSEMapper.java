/**
 * 
 */
package org.gcube.vremanagement.executor.json;

import org.gcube.com.fasterxml.jackson.annotation.JsonTypeInfo;
import org.gcube.common.authorization.library.provider.ClientInfo;
import org.gcube.common.authorization.library.provider.ContainerInfo;
import org.gcube.common.authorization.library.provider.ExternalServiceInfo;
import org.gcube.common.authorization.library.provider.ServiceInfo;
import org.gcube.common.authorization.library.provider.UserInfo;
import org.gcube.vremanagement.executor.scheduledtask.ScheduledTask;

/**
 * @author Luca Frosini (ISTI - CNR)
 *
 */
public class ExtendedSEMapper extends SEMapper {
	
	protected static ExtendedSEMapper instance;
	
	static {
		instance = new ExtendedSEMapper();
	}
	
	public static ExtendedSEMapper getInstance() {
		return instance;
	}
	
	
	@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property=SEMapper.CLASS_PROPERTY)
	class ClientInfoMixIn {
		
	}
	
	
	ExtendedSEMapper() {
		super();
		
		mapper.registerSubtypes(ScheduledTask.class);
		
		mapper.addMixIn(ClientInfo.class, ClientInfoMixIn.class);
		mapper.registerSubtypes(UserInfo.class);
		mapper.registerSubtypes(ServiceInfo.class);
		mapper.registerSubtypes(ExternalServiceInfo.class);
		mapper.registerSubtypes(ContainerInfo.class);
		
	}
	
	

}
