package org.gcube.vremanagement.executor.pluginmanager;

import java.util.HashMap;
import java.util.Map;
import java.util.ServiceLoader;
import java.util.UUID;

import org.gcube.vremanagement.executor.exception.ExecutorException;
import org.gcube.vremanagement.executor.exception.PluginNotFoundException;
import org.gcube.vremanagement.executor.plugin.Plugin;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This is a singleton class which discover on classpath the available plugins
 * and map the plugin name to its implementation class.
 * The plugin implementation class can be retrieved using its name.
 * @author Luca Frosini (ISTI - CNR)
 */
public class PluginManager {

	/**
	 * Logger
	 */
	private static Logger logger = LoggerFactory.getLogger(PluginManager.class);
	
	/** 
	 * Singleton instance
	 */
	private static PluginManager pluginManager;
	
	/**
	 * Contains mapping between plugin name and the instance of its declaration
	 * class 
	 */
	private Map<String, Class<? extends Plugin>> availablePlugins;
	
	private Map<String, UUID> uuids;
	
	/**
	 * Get the singleton instance of {@link #PluginManager}.
	 * The first time this function is invoked the instance is null
	 * so it is created. Otherwise the already created instance is returned
	 * @return singleton instance of {@link #PluginManager}
	 */
	public synchronized static PluginManager getInstance(){
		if(pluginManager== null){
			pluginManager = new PluginManager();
		}
		return pluginManager;
	}
	
	/**
	 * Used by {@link #getInstance()} function check the available plugin on classpath
	 * and add them on {@link #availablePlugins}
	 */
	protected PluginManager(){
		logger.debug("Loading plugins available on classpath");
		this.availablePlugins = new HashMap<>();
		this.uuids = new HashMap<>();
		ServiceLoader<Plugin> serviceLoader = ServiceLoader.load(Plugin.class);
		for (Plugin plugin : serviceLoader) {
			try {
				logger.debug(String.format("%s plugin found", plugin.getName()));
				String name = plugin.getName();
				this.availablePlugins.put(name, plugin.getClass());
				this.uuids.put(name, UUID.randomUUID());
			} catch (Exception e) {
				logger.debug(String.format("%s not initialized correctly. It will not be used", plugin.getName()));
			}
		}
	}
	
	/**
	 * 
	 * @param pluginName The name of the plugin
	 * @return The plugin declaration if available, null otherwise
	 * @throws PluginNotFoundException 
	 */
	public Plugin getPlugin(String pluginName) throws PluginNotFoundException, ExecutorException {
		Class<? extends Plugin> pluginClass = getAvailablePlugins().get(pluginName);
		if (pluginClass== null) {
			throw new PluginNotFoundException("Plugin " + pluginName + " not available in this smart-executor instance");
		}
		try {
			return pluginClass.getDeclaredConstructor().newInstance();
		}catch (Exception e) {
			throw new ExecutorException("Unable to instatiate plugin " + pluginName, e);
		}
	}
	
	
	public UUID getPluginUUID(String pluginName) {
		return uuids.get(pluginName);
	}
	
	/**
	 * @return the availablePlugins
	 */
	public Map<String, Class<? extends Plugin>> getAvailablePlugins() {
		return availablePlugins;
	}
	
	
}
