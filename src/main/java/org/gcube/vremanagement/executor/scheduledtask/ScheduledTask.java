/**
 * 
 */
package org.gcube.vremanagement.executor.scheduledtask;

import java.util.UUID;

import org.gcube.com.fasterxml.jackson.annotation.JsonTypeInfo;
import org.gcube.common.authorization.library.provider.ClientInfo;
import org.gcube.common.authorization.utils.manager.SecretManager;
import org.gcube.common.authorization.utils.manager.SecretManagerProvider;
import org.gcube.common.authorization.utils.secret.Secret;
import org.gcube.common.resources.gcore.GCoreEndpoint;
import org.gcube.common.resources.gcore.GCoreEndpoint.Profile.Endpoint;
import org.gcube.common.resources.gcore.HostingNode;
import org.gcube.common.resources.gcore.utils.Group;
import org.gcube.smartgears.Constants;
import org.gcube.smartgears.ContextProvider;
import org.gcube.vremanagement.executor.api.types.LaunchParameter;
import org.gcube.vremanagement.executor.json.ExtendedSEMapper;
import org.gcube.vremanagement.executor.json.SEMapper;
import org.gcube.vremanagement.executor.plugin.Ref;
import org.gcube.vremanagement.executor.plugin.RunOn;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = SEMapper.CLASS_PROPERTY)
public class ScheduledTask extends org.gcube.vremanagement.executor.plugin.ScheduledTask {
	
	protected String token;
	protected String context;
	protected ClientInfo clientInfo;
	
	protected ScheduledTask() {
	}
	
	public ScheduledTask(UUID uuid, LaunchParameter launchParameter) throws Exception {
		this(uuid, launchParameter, generateRunOn());
	}
	
	public ScheduledTask(UUID uuid, LaunchParameter launchParameter, RunOn runOn) throws Exception {
		this.uuid = uuid;
		this.launchParameter = launchParameter;
		SecretManager secretManager = SecretManagerProvider.instance.get();
		Secret secret = secretManager.getCurrentSecretHolder().getSecrets().first();
		this.token = secret.getToken();
		this.context = secretManager.getContext();
		this.clientInfo = secret.getClientInfo();
		this.runOn = runOn;
	}
	
	/**
	 * @return the token
	 */
	public String getToken() {
		return token;
	}
	
	/**
	 * @return the scope
	 */
	public String getContext() {
		return context;
	}
	
	/**
	 * @return the clientInfo
	 */
	public ClientInfo getClientInfo() {
		return clientInfo;
	}
	
	public static final String LOCALHOST = "localhost";
	
	public static RunOn generateRunOn() {
		Ref hostingNodeRef = null;
		try {
			HostingNode hostingNode = ContextProvider.get().container().profile(HostingNode.class);
			hostingNodeRef = new Ref(hostingNode.id(), hostingNode.profile().description().name());
		} catch(Exception e) {
			// 
			hostingNodeRef = new Ref(LOCALHOST, LOCALHOST);
		}
		
		Ref eServiceRef = null;
		try {
			GCoreEndpoint gCoreEndpoint = ContextProvider.get().profile(GCoreEndpoint.class);
			String address = "";
			Group<Endpoint> endpoints = gCoreEndpoint.profile().endpoints();
			for(Endpoint endpoint : endpoints) {
				if(endpoint.name().contains(Constants.remote_management)) {
					continue;
				} else {
					address = endpoint.uri().toString();
					break;
				}
			}
			
			eServiceRef = new Ref(gCoreEndpoint.id(), address);
		} catch(Exception e) {
			eServiceRef = new Ref(LOCALHOST, LOCALHOST);
		}
		
		RunOn runOn = new RunOn(hostingNodeRef, eServiceRef);
		
		return runOn;
	}
	
	@Override
	public String toString() {
		try {
			return ExtendedSEMapper.getInstance().marshal(this);
		} catch(Exception e) {
			return "ScheduledTask [token=" + token + ", context=" + context + ", clientInfo=" + clientInfo + ", uuid="
					+ uuid + ", launchParameter=" + launchParameter + ", runOn=" + runOn + "]";
		}
	}
	
}
