/**
 * 
 */
package org.gcube.vremanagement.executor;


import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.gcube.common.authorization.utils.manager.SecretManagerProvider;
import org.gcube.common.resources.gcore.Resource;
import org.gcube.common.resources.gcore.ServiceEndpoint;
import org.gcube.informationsystem.publisher.RegistryPublisher;
import org.gcube.informationsystem.publisher.RegistryPublisherFactory;
import org.gcube.resources.discovery.client.api.DiscoveryClient;
import org.gcube.resources.discovery.client.queries.api.SimpleQuery;
import org.gcube.resources.discovery.icclient.ICFactory;
import org.gcube.vremanagement.executor.json.ExtendedSEMapper;
import org.gcube.vremanagement.executor.plugin.Plugin;
import org.gcube.vremanagement.executor.plugin.PluginDefinition;
import org.gcube.vremanagement.executor.pluginmanager.PluginManager;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Luca Frosini (ISTI - CNR)
 *
 */
public class SmartExecutorInizializatorTest {
	
	private static Logger logger = LoggerFactory.getLogger(SmartExecutorInizializatorTest.class);

	@Test
	public void getAvailablePlugin() throws Exception {
		PluginManager pluginManager = PluginManager.getInstance();
		Map<String, Class<? extends Plugin>> availablePlugins = pluginManager.getAvailablePlugins();
		List<PluginDefinition> plugins = new ArrayList<>();
		for(String pluginName : availablePlugins.keySet()) {
			Plugin plugin = pluginManager.getPlugin(pluginName);
			plugins.add(plugin);
			logger.debug("Plugin {}, Class {}", pluginName, availablePlugins.get(pluginName).getSimpleName());
		}
		
		logger.debug("{}", ExtendedSEMapper.getInstance().marshal(PluginDefinition.class, plugins));
	}
	
	protected static void unPublishResource(Resource resource) throws Exception {
		//StringWriter stringWriter = new StringWriter();
		//Resources.marshal(resource, stringWriter);
		
		RegistryPublisher registryPublisher = RegistryPublisherFactory.create();
	    
		String id = resource.id();
		logger.debug("Trying to remove {} with ID {} from {}", resource.getClass().getSimpleName(), id, SecretManagerProvider.instance.get().getContext());
	    
		registryPublisher.remove(resource);
		
		logger.debug("{} with ID {} removed successfully", resource.getClass().getSimpleName(), id);
	}
	
	// @Test
	public void removeOldInstances() throws Exception{
		try {
			//ContextTest.setContextByName("/d4science.research-infrastructures.eu");
			
			SimpleQuery query = ICFactory.queryFor(ServiceEndpoint.class)
			.addCondition(String.format("$resource/Profile/Category/text() eq '%s'", "VREManagement"))
			.addCondition(String.format("$resource/Profile/Name/text() eq '%s'", "SmartExecutor"))
			.addCondition(String.format("$resource/Profile/AccessPoint/Interface/Endpoint/@EntryName eq '%s'", "SmartGenericWorker"));
			 
			DiscoveryClient<ServiceEndpoint> client = ICFactory.clientFor(ServiceEndpoint.class);
			List<ServiceEndpoint> serviceEndpoints = client.submit(query);
			
			for (ServiceEndpoint serviceEndpoint : serviceEndpoints) {
				try {
					logger.debug("Trying to unpublish the old ServiceEndpoint with ID {} ({}) from scope {}", 
							serviceEndpoint.id(), serviceEndpoint.profile().runtime().hostedOn(), ContextUtility.getCurrentContext());
					// unPublishResource(serviceEndpoint);
				} catch(Exception e){
					logger.debug("Exception tryng to unpublish the old ServiceEndpoint with ID {} ({}) from scope {}", 
							serviceEndpoint.id(), serviceEndpoint.profile().runtime().hostedOn(), ContextUtility.getCurrentContext(), e);
				}
			}
		}catch(Exception e){
			logger.debug("An Exception occur while checking and/or unpublishing old ServiceEndpoint", e);
		}
	}
}
