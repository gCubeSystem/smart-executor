/**
 * 
 */
package org.gcube.vremanagement.executor;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.gcube.com.fasterxml.jackson.core.JsonGenerationException;
import org.gcube.com.fasterxml.jackson.databind.JsonMappingException;
import org.gcube.com.fasterxml.jackson.databind.ObjectMapper;
import org.gcube.vremanagement.executor.api.types.LaunchParameter;
import org.gcube.vremanagement.executor.api.types.Scheduling;
import org.gcube.vremanagement.executor.exception.InvalidPluginStateEvolutionException;
import org.gcube.vremanagement.executor.json.ExtendedSEMapper;
import org.gcube.vremanagement.executor.json.SEMapper;
import org.gcube.vremanagement.executor.plugin.Plugin;
import org.gcube.vremanagement.executor.plugin.PluginDefinition;
import org.gcube.vremanagement.executor.plugin.PluginState;
import org.gcube.vremanagement.executor.plugin.PluginStateEvolution;
import org.gcube.vremanagement.executor.plugin.Ref;
import org.gcube.vremanagement.executor.plugin.RunOn;
import org.gcube.vremanagement.executor.pluginmanager.PluginManager;
import org.gcube.vremanagement.executor.scheduledtask.ScheduledTask;
import org.gcube.vremanagement.helloworld.HelloWorldPlugin;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Luca Frosini (ISTI - CNR)
 *
 */
public class SerializationTest extends ContextTest {
	
	private static Logger logger = LoggerFactory.getLogger(SerializationTest.class);
	
	@Test
	public void testScheduling() throws JsonGenerationException, JsonMappingException, IOException {
		Map<String,Object> inputs = new HashMap<String,Object>();
		inputs.put("Hello", "World");
		long sleepTime = 10000;
		inputs.put("sleepTime", sleepTime);
		
		Scheduling scheduling = new Scheduling(20);
		scheduling.setGlobal(true);
		
		LaunchParameter launchParameter = new LaunchParameter("HelloWorld", inputs, scheduling);
		logger.debug("{} to be Marshalled : {}", launchParameter.getClass().getSimpleName(), launchParameter);
		
		ObjectMapper objectMapper = new ObjectMapper();
		String launchParameterJSONString = objectMapper.writeValueAsString(launchParameter);
		logger.debug("Marshalled : {}", launchParameterJSONString);
		
		LaunchParameter launchParameterUnmarshalled = objectMapper.readValue(launchParameterJSONString,
				LaunchParameter.class);
		logger.debug("UnMarshalled : {}", launchParameterUnmarshalled);
	}
	
	@Test
	public void testScheduledTask() throws Exception {
		Map<String,Object> inputs = new HashMap<String,Object>();
		inputs.put("Hello", "World");
		long sleepTime = 10000;
		inputs.put("sleepTime", sleepTime);
		
		Scheduling scheduling = new Scheduling(20);
		scheduling.setGlobal(true);
		
		LaunchParameter launchParameter = new LaunchParameter("HelloWorld", inputs, scheduling);
		UUID uuid = UUID.randomUUID();
		Ref hostingNode = new Ref(UUID.randomUUID().toString(), "localhost");
		Ref eService = new Ref(UUID.randomUUID().toString(), "localhost");
		RunOn runOn = new RunOn(hostingNode, eService);
		ScheduledTask scheduledTask = new ScheduledTask(uuid, launchParameter, runOn);
		logger.debug("{} to be Marshalled : {}", scheduledTask.getClass().getSimpleName(), launchParameter);
		
		String scheduledTaskJSONString = ExtendedSEMapper.getInstance().marshal(scheduledTask);
		logger.debug("Marshalled : {}", scheduledTaskJSONString);
		
		ScheduledTask scheduledTaskUnmarshalled = ExtendedSEMapper.getInstance().unmarshal(ScheduledTask.class,
				scheduledTaskJSONString);
		logger.debug("UnMarshalled : {}", scheduledTaskUnmarshalled);
		
	}
	
	@Test
	public void testPluginEvolutionState()
			throws JsonGenerationException, JsonMappingException, IOException, InvalidPluginStateEvolutionException {
		
		PluginStateEvolution pes = new PluginStateEvolution(UUID.randomUUID(), 1,
				Calendar.getInstance().getTimeInMillis(), new HelloWorldPlugin(), PluginState.RUNNING, 10);
		logger.debug("{} to be Marshalled : {}", pes.getClass().getSimpleName(), pes);
		
		ObjectMapper objectMapper = new ObjectMapper();
		String scheduledTaskJSONString = objectMapper.writeValueAsString(pes);
		logger.debug("Marshalled : {}", scheduledTaskJSONString);
		
		PluginStateEvolution pesUnmarshalled = objectMapper.readValue(scheduledTaskJSONString,
				PluginStateEvolution.class);
		logger.debug("UnMarshalled : {}", pesUnmarshalled);
	}
	
	@Test
	public void testAvailablePluginMarshalling() throws Exception {
		PluginManager pluginManager = PluginManager.getInstance();
		Map<String, Class<? extends Plugin>> availablePlugins = pluginManager.getAvailablePlugins();
		List<PluginDefinition> plugins = new ArrayList<>();
		for(String pluginName : availablePlugins.keySet()) {
			plugins.add(pluginManager.getPlugin(pluginName));
		}
		String list = ExtendedSEMapper.getInstance().marshal(PluginDefinition.class, plugins);
		logger.debug("Plugins are :\n{}", list);
	}
	
	@Test
	public void test() throws Exception {
		String scheduled = "[{" + "\"@class\":\"ScheduledTask\"," + "\"uuid\":\"d9d9bc94-b1ac-43e2-ada7-0da8045b637f\","
				+ "\"scope\":\"/gcube/devNext/NextNext\","
				+ "\"token\":\"abcdef-7f6e-49cd-9a34-909cd3832f3e-98187548\"," + "\"clientInfo\":{"
				+ "		\"@class\":\"UserInfo\",\"roles\":[\"OrganizationMember\"],\"id\":null,\"type\":\"USER\"" + "},"
				+ "\"runOn\":{" + "		\"@class\":\"RunOn\"," + "		\"hostingNode\":{"
				+ "			\"@class\":\"Ref\",\"id\":\"5f51c684-dd23-4d0e-a19d-9710ce34056a\","
				+ "			\"address\":\"pc-frosini.isti.cnr.it:8080\"" + "		}," + "		\"eService\":{"
				+ "			\"@class\":\"Ref\",\"id\":\"5f51c684-dd23-4d0e-a19d-9710ce34056a\","
				+ "			\"address\":\"pc-frosini.isti.cnr.it:8080\"" + "		}" + "}," + "\"launchParameter\":{"
				+ "		\"@class\":\"LaunchParameter\",\"pluginCapabilities\":null," + "		\"inputs\":{"
				+ "			\"Hello\":\"World\",\"sleepTime\":10000" + "		},"
				+ "		\"pluginStateNotifications\":{" + "			\"org.acme.HWPluginStateNotification\":{"
				+ "				\"Hello\":\"Hello World Notification :) :)\"" + "		}},"
				+ "		\"scheduling\":{" + "			\"@class\":\"Scheduling\",\"cronExpression\":null,"
				+ "			\"delay\":120,\"schedulingTimes\":4,\"firstStartTime\":null,\"endTime\":null,"
				+ "			\"previuosExecutionsMustBeCompleted\":true,\"global\":false" + "		},"
				+ "		\"pluginName\":\"HelloWorld\"," + "		\"pluginVersion\":null" + "	}" + "}]";
		List<ScheduledTask> scheduledTasks = ExtendedSEMapper.getInstance().unmarshalList(ScheduledTask.class,
				scheduled);
		
		String complete = ExtendedSEMapper.getInstance().marshal(ScheduledTask.class, scheduledTasks);
		logger.debug("ExtendedSEMapper : {}", complete);
		
		List<org.gcube.vremanagement.executor.plugin.ScheduledTask> tasks = new ArrayList<>();
		for(ScheduledTask scheduledTask : scheduledTasks) {
			org.gcube.vremanagement.executor.plugin.ScheduledTask task = new org.gcube.vremanagement.executor.plugin.ScheduledTask(
					scheduledTask.getUUID(), scheduledTask.getRunOn(), scheduledTask.getLaunchParameter());
			tasks.add(task);
		}
		
		String marshalled = SEMapper.getInstance().marshal(org.gcube.vremanagement.executor.plugin.ScheduledTask.class,
				tasks);
		logger.debug("SEMapper : {}", marshalled);
	}
	
}
