package org.gcube.vremanagement.executor.ispublisher;

import java.net.URI;
import java.util.UUID;

import javax.servlet.ServletContext;

import org.gcube.common.events.Hub;
import org.gcube.informationsystem.serialization.ElementMapper;
import org.gcube.resourcemanagement.model.impl.entities.facets.AccessPointFacetImpl;
import org.gcube.resourcemanagement.model.reference.entities.facets.AccessPointFacet;
import org.gcube.smartgears.configuration.application.ApplicationConfiguration;
import org.gcube.smartgears.context.Properties;
import org.gcube.smartgears.context.application.ApplicationContext;
import org.gcube.smartgears.context.container.ContainerContext;
import org.gcube.smartgears.lifecycle.application.ApplicationLifecycle;
import org.gcube.smartgears.persistence.Persistence;
import org.gcube.vremanagement.executor.ContextTest;
import org.gcube.vremanagement.executor.plugin.Plugin;
import org.gcube.vremanagement.helloworld.HelloWorldPlugin;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public class RestISPublisherTest extends ContextTest {

	private static Logger logger = LoggerFactory.getLogger(RestISPublisherTest.class);
	
	protected ApplicationContext applicationContext;
	
	public RestISPublisherTest() {
		applicationContext = new ApplicationContext() {
			
			@Override
			public Properties properties() {
				return null;
			}
			
			@Override
			public <T> T profile(Class<T> type) {
				return null;
			}
			
			@Override
			public Persistence persistence() {
				return null;
			}
			
			@Override
			public String name() {
				return "mock";
			}
			
			@Override
			public ApplicationLifecycle lifecycle() {
				return null;
			}
			
			@Override
			public String id() {
				return UUID.randomUUID().toString();
			}
			
			@Override
			public Hub events() {
				return null;
			}
			
			@Override
			public ContainerContext container() {
				return null;
			}
			
			@Override
			public ApplicationConfiguration configuration() {
				return null;
			}
			
			@Override
			public ServletContext application() {
				return null;
			}
		};
	}
	
	@Test
	public void testPluginEndpoint() throws Exception {
		RestISPublisher restISPublisher = new RestISPublisher(applicationContext);
		Plugin plugin = new HelloWorldPlugin();
		
		AccessPointFacet smartExecutorAccessPointFacet = new AccessPointFacetImpl();
		smartExecutorAccessPointFacet.setEndpoint(new URI("https://smart-executor.d4science.org/smart-executor"));
		
		AccessPointFacet accessPointFacet = restISPublisher.getPluginAccessPointFacet(plugin, smartExecutorAccessPointFacet);
		logger.debug("Plugin URI is {}", ElementMapper.marshal(accessPointFacet));
	}
	
}
