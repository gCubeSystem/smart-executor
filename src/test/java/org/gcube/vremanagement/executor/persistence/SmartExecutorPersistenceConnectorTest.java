/**
 * 
 */
package org.gcube.vremanagement.executor.persistence;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import org.gcube.vremanagement.executor.ContextTest;
import org.gcube.vremanagement.executor.persistence.orientdb.OrientDBPersistenceConnector;
import org.gcube.vremanagement.executor.plugin.PluginState;
import org.gcube.vremanagement.executor.plugin.PluginStateEvolution;
import org.gcube.vremanagement.executor.scheduledtask.ScheduledTask;
import org.gcube.vremanagement.executor.scheduledtask.ScheduledTaskPersistence;
import org.gcube.vremanagement.executor.scheduledtask.ScheduledTaskPersistenceFactory;
import org.gcube.vremanagement.helloworld.HelloWorldPlugin;
import org.junit.Assert;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Luca Frosini (ISTI - CNR)
 *
 */
public class SmartExecutorPersistenceConnectorTest extends ContextTest {
	
	private static Logger logger = LoggerFactory.getLogger(SmartExecutorPersistenceConnectorTest.class);
	
	@Test
	public void getConnectionTest() throws Exception {
		// ContextTest.setContextByName(ROOT);
		SmartExecutorPersistenceConnector persistenceConnector = SmartExecutorPersistenceFactory.getPersistenceConnector();
		Assert.assertNotNull(persistenceConnector);
		Assert.assertEquals(OrientDBPersistenceConnector.class, persistenceConnector.getClass());
		SmartExecutorPersistenceFactory.closeCurrentPersistenceConnector();
	}
	
	@Test
	public void getPluginInstanceStateTest() throws Exception {
		SmartExecutorPersistenceConnector persistenceConnector = SmartExecutorPersistenceFactory.getPersistenceConnector();
		UUID uuid  = UUID.randomUUID();
		
		PluginState[] states = PluginState.values();
		
		for(int i=0; i<states.length; i++){
			long timestamp =  new Date().getTime();
			HelloWorldPlugin helloWorldPlugin = new HelloWorldPlugin();
			PluginStateEvolution pluginStateEvolution = new PluginStateEvolution(uuid, 1, timestamp, helloWorldPlugin, states[i], 0);
			persistenceConnector.pluginStateEvolution(pluginStateEvolution, null);
			
			long startTime = Calendar.getInstance().getTimeInMillis();
			long endTime = startTime;
			while(endTime <=  (startTime + 1000)){
				endTime = Calendar.getInstance().getTimeInMillis();
			}
			
			PluginStateEvolution pse = persistenceConnector.getPluginInstanceState(uuid, 1);
			PluginState ps = pse.getPluginState();
			Assert.assertEquals(states[i], ps);
		}
		
		PluginStateEvolution pse = persistenceConnector.getPluginInstanceState(uuid, null);
		PluginState ps = pse.getPluginState();
		Assert.assertEquals(states[states.length-1], ps);
		
		SmartExecutorPersistenceFactory.closeCurrentPersistenceConnector();
	}
	
	@Test
	public void getAvailableScheduledTasksTest() throws Exception {
		ContextTest.setContextByName("/gcube/devsec");
		ScheduledTaskPersistence stc = ScheduledTaskPersistenceFactory.getScheduledTaskPersistence();
		Assert.assertNotNull(stc);
		Assert.assertEquals(OrientDBPersistenceConnector.class, stc.getClass());
		
		List<String> plugins = new ArrayList<>();
		plugins.add("hello-world-se-plugin");
		List<ScheduledTask> lc = stc.getScheduledTasks(plugins);
		
		logger.debug("Available Scheduled Tasks : {}", lc);
	}
	
	
}
