/**
 * 
 */
package org.gcube.vremanagement.executor;


import java.util.HashMap;
import java.util.Map;

import org.gcube.vremanagement.executor.plugin.Plugin;
import org.gcube.vremanagement.executor.pluginmanager.PluginManager;
import org.gcube.vremanagement.helloworld.HelloWorldPlugin;
import org.junit.Test;

/**
 * @author Luca Frosini (ISTI - CNR)
 *
 */
public class SmartExecutorImplTest {
	
	@Test
	public void helloWorldTest() throws Exception{
		Map<String, Object> inputs = new HashMap<String, Object>();
		long sleepTime = 10000;
		inputs.put(HelloWorldPlugin.SLEEP_TIME, sleepTime);
		String name = (new HelloWorldPlugin()).getName();
		PluginManager pluginManager = PluginManager.getInstance();
		Plugin plugin = pluginManager.getPlugin(name);
		plugin.launch(inputs);
	}
	
}
