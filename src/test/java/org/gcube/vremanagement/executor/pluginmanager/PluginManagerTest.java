package org.gcube.vremanagement.executor.pluginmanager;

import java.util.UUID;

import org.gcube.vremanagement.executor.ContextTest;
import org.gcube.vremanagement.executor.exception.ExecutorException;
import org.gcube.vremanagement.executor.exception.PluginNotFoundException;
import org.gcube.vremanagement.helloworld.HelloWorldPlugin;
import org.junit.Assert;
import org.junit.Test;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public class PluginManagerTest extends ContextTest {

	@Test
	public void getHelloWorldPlugin() throws ExecutorException {
		PluginManager pluginManager = PluginManager.getInstance();
		Assert.assertEquals(HelloWorldPlugin.class, pluginManager.getPlugin(new HelloWorldPlugin().getName()).getClass());
		try {
			pluginManager.getPlugin(UUID.randomUUID().toString());
		}catch (PluginNotFoundException e) {
			// Ok. This is the expected behaviour
		}
	}
	
}
