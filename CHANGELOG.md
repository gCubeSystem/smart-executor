This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

# Changelog for Smart Executor Service

## [v3.2.1-SNAPSHOT]

- Fixed bug on concurrent attempt to enter in a session while trying to schedule a second task #27319
- Fixed bug on concurrent attempt to enter in a session while trying to execute another task #24116
- Upgraded gcube-smartgears-bom to 2.6.0-SNAPSHOT


## [v3.2.0]

- Fixed RequestFilter to avoid to remove info to Smartgears
- Migrated code to reorganized E/R format [#24992]
- Force guava version to 23.6-jre to meet requirements of new plugins


## [v3.1.0]

- Ported service to authorization-utils [#22871]


## [v3.0.0] 

- Switched smart-executor JSON management to gcube-jackson [#19647]
- Migrated Code from OrientDB 2.2.X APIs OrientDB 3.0.X APIs [#16123]
- Redesigned HTTP APIs to comply with RESTful architectural style [#12997]
- Added API to retrieve scheduled tasks [#10780]


## [v2.0.0] 

- Removed SOAP APIs


## [v1.9.0] [r4.10.0] - 2018-02-15

- Added REST interface to Smart Executor [#5109]


## [v1.8.0] [r4.8.0] - 2017-11-29

- Fixed exception on stop() method [#10064]


## [v1.7.0] [r4.7.0] - 2017-10-09

- Changed JobUsageRecord use due to changes in model [#9646]
- Removed TaskUsageRecord use due to changes in model [#9647]


## [v1.6.0] [r4.6.0] - 2017-07-25

- Setting explicitly the context before running the plugin to avoid wrong context made from quartz thread reuse


## [v1.5.0] [r4.3.0] - 2017-03-16

- Provided access to UUID and iteration number for a plugin [#6733]
- Added support to implements Reminiscence for a Scheduled Task [#772]
- Removed configuration file support added in release 1.2.0 now available via Reminiscence [#772]
- Migrated from CouchDB® to OrientDB® [#6565]
- Added accounting by using TaskUsageRecord [#6116]


## [v1.4.0] [r4.1.0] - 2016-11-07

- SmartExecutor has been migrated to Authorization 2.0 [#4944] [#2112]
- Provided to plugins the possibility to specify progress percentage [#440]
- Provided to plugins the possibility to define a custom notifier [#5089]


## [v1.3.0] [r3.10.0] - 2016-02-08

- Using CouchDB® to save Scheduled Task configuration [#579]
- Added Unscheduling feature for repetitive task [#521]


## [v1.2.0] [r3.9.1] - 2015-12-31

- Removed Support for configuration File to run task at service startup


## [v1.1.0] [r3.9.0] - 2015-12-09

- Added Support for configuration File to run task at service startup [#508]
- Added Recurrent and Scheduled Task support [#111]
- Saving Task Evolution on Persistence CouchDB® [#89]


## [v1.0.0] - 2015-02-05

- First Release

