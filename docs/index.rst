
***********************************************************
Welcome to Smart Executor Service documentation
***********************************************************

Smart Executor is a RESTful application which exposes operations via REST-API.

See the available `REST-API docs <../api-docs/index.html>`_.

Authorization
=============

D4Science adopts state-of-the-art industry standards for authentication and authorization. 
Specifically, the implementation fully adopts `OIDC (OpenID Connect) <https://openid.net/connect>`_ for authentication and UMA 2 (User-Managed Authorization) for authorization flows. 
`JSON Web Token (JWT) Access token <https://jwt.io/>`_ are used for both authentication and authorization.

Obtain your Bearer token here: https://dev.d4science.org/how-to-access-resources

Service
=======

You can call the methods of the Web Service by writing your own REST client application or using existing REST client plugins.


HTTP Statuses
-------------

Any successful operation returns *200 OK* HTTP status code.
The create operation returns *201 Created*. 
Any Background operation returns *202 Accepted*.
Any operation which does not provide any content return *204 No Content*.



The most common error status a client can obtain are:

* **400 Bad Request** used to indicate a clients error `<https://tools.ietf.org/html/rfc7231#section-6.5.1>`_;
* **401 Unauthorized** used to indicate that the client does not provide the authorization token in the HTTP Header or the client has not enough right to perform such request `<https://tools.ietf.org/html/rfc7235#section-3.1>`_;
* **404 Not Found** used to indicate that the requested instance does not exist `<https://tools.ietf.org/html/rfc7231#section-6.5.4>`_;
* **405 Method Not Allowed** the used HTTP method is not supported for the requested URL `<https://tools.ietf.org/html/rfc7231#section-6.5.5>`_.
  The response contains the *Allow* HTTP Header indicating the supported HTTP method for such URL `<https://tools.ietf.org/html/rfc7231#section-7.4.1>`_;
* **409 Conflict**  the request could not be completed due to a conflict with the current state of the target resource (e.g. the name of the resource already exists) `<https://tools.ietf.org/html/rfc7231#section-6.5.8>`_;
* **500 Internal Server Error** indicate a server failure `<https://tools.ietf.org/html/rfc7231#section-6.6.1>`_.

You can find a complete list of HTTP Status at `<https://httpstatuses.com/>`_

If you get a *500 Internal Server Error*, please report it in the `gCube ticketing system <https://support.d4science.org>`_. 

Please use this checklist before reporting an error:

* Replicate the request;
* The failure could be temporal due to network error, server issue and many other temporal issues. For this reason, please retry the request after a certain amount of time before reporting the issue;
* indicate how to replicate the error;
* indicate the time when the error occurred (this simplifies identifying the issue).

HTTP Methods
------------

Smart Executor is a pure RESTful service. 



Service Discovery on IS
=======================

The service can be discovered in the gCore IS as gCore Endpoint with the following parameter:

.. code:: xml

	<ServiceClass>org.gcube.vremanagement</ServiceClass>
	<ServiceName>smart-executor</ServiceName>


The service can be discovered in the Facet Based IS as EService with the following json query:

.. code:: json

	{
		"@class": "EService",
		"consistsOf": [
			{
				"@class": "IsIdentifiedBy",
				"target": {
					"@class": "SoftwareFacet",
					"group": "org.gcube.vremanagement",
					"name": "smart-executor"
				}
			}
		]
	}

	
	
Service Maven Coordinates
=========================
	
The maven coordinates of gCat service are:

.. code:: xml

	<groupId>org.gcube.vremanagement</groupId>
	<artifactId>smart-executor</artifactId>
	